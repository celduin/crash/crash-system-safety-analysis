- Scenario Constraint:
  Control Action: Python Send Images to model (PSI)

  - Reference: Refined-Constraint-PSI-1
    Text: Python script must produce the images in a suitable form, with enough resolvable detail, for use by the model.

  - Reference: Refined-Constraint-PSI-2
    Text: There must be > X KB free image space at all times.

  - Reference: Refined-Constraint-PSI-3
    Text: If there is < X KB image space, the system must stop.

  - Reference: Refined-Constraint-PSI-4
    Text: Images from GStreamer must be in the agreed format type.

  - Reference: Refined-Constraint-PSI-5
    Text: If GStreamer has a config which stops, or could stop, the Python script from detecting new images, there must be a 'systemctl restart'.

  - Reference: Refined-Constraint-PSI-6
    Text: Images must be in app sync format when being sent from GStreamer to Python script.

  - Reference: Refined-Constraint-PSI-7
    Text: If there is a library bug which stops, or could stop, the Python script from detecting new images, the motors must stop and there must be a library update and system restart.

  - Reference: Refined-Constraint-PSI-8
    Text: GStreamer and OpenCV must always get updated to the correct versions within X seconds, but not updated to incorrect versions.

  - Reference: Refined-Constraint-PSI-9
    Text: Each image must have a unique label.

  - Reference: Refined-Constraint-PSI-10
    Text: The GStreamer and Python timestamps of the images must be consistent.

  - Reference: Refined-Constraint-PSI-11
    Text: If the images become poor quality the motors must stop within X seconds and resume within Y seconds of the image quality recovering.

  - Reference: Refined-Constraint-PSI-12
    Text: If the room is too dark or bright, the Python script must resolve the definition of the image so that it is consistent with the expectations of the model.

  - Reference: Refined-Constraint-PSI-13
    Text: The Kernel scheduler must be able to handle the flow of images without issues, or else when kernel scheduler issues arise there must be a 'systemctl' restart.

  - Reference: Refined-Constraint-PSI-14
    Text: If the GPU is locked up the system must stop.

  - Reference: Refined-Constraint-PSI-15
    Text: Any image filesize must be < X KB

  - Reference: Refined-Constraint-PSI-16
    Text: When images are sent to it, the model must provide object instance and steering instructions feedback.

  - Reference: Refined-Constraint-PSI-17
    Text: In the case of a Cuda error discarding model feedback, the motors and system must stop.

  - Reference: Refined-Constraint-PSI-18
    Text: The model must not take more than X seconds to produce output after receiving images.

  - Reference: Refined-Constraint-PSI-19
    Text: If the model does take too long to process the image into instructions the motors must stop.


- Scenario Constraint:
  Control Action: Python send Go command to GStreamer (GG)

  - Reference: Refined-Constraint-GG-1
    Text: The Go cmd config file that Python Script gets from BuildStream must be a valid config which GStreamer can use to run.

  - Reference: Refined-Constraint-GG-2
    Text: Python script must be able to read any config file from Buildstream correctly.

  - Reference: Refined-Constraint-GG-3
    Text: Python script must always restart the system when GStreamer is not using a good config file.

  - Reference: Refined-Constraint-GG-4
    Text: Buildstream and Python script must have correct GStreamer config requirements if the last requirement change was more than X seconds ago.

  - Reference: Refined-Constraint-GG-5
    Text: The Kernel scheduler must be able to execute the Go cmd within X seconds without issues, or else when kernel scheduler issues arise there must be a 'systemctl' restart.

  - Reference: Refined-Constraint-GG-6
    Text: GStreamer must be in a receptive state when the system is started by the time Python script sends the Go cmd.

  - Reference: Refined-Constraint-GG-7
    Text: The Go cmd from Python script must be a valid config which GStreamer can use to run.

  - Reference: Refined-Constraint-GG-8
    Text: The GStreamer pipeline must be constructed so that it provides environment images when it has received the Go cmd.

  - Reference: Refined-Constraint-GG-9
    Text: GStreamer must stop properly when the Python Script sends the exit command.

  - Reference: Refined-Constraint-GG-10
    Text: GStreamer, with any OpenCV image reformatting included, must send images to Python script within X seconds of the camera sending them.

  - Reference: Refined-Constraint-GG-11
    Text: The GStreamer pipeline (with its bitrate, blocksize, image resolution settings) must not stutter.
