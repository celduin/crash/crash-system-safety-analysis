digraph G {
compound=true

node [shape=box, width=4, fontname=arial, fontsize=24]
graph [splines=ortho, ranksep=1.5, nodesep=0.8]
edge [fontname=arial, fontsize = 11, style="setlinewidth(1)", lp=10, overlap=false]
layout=dot; overlap=false;

"Technician"[width=15]
"System Integration Pipeline"[width=17]
"System D"[width=18]
"OSTree Management Service"[width=6]
"Obstacle Model"[width=3]
"Steering Model"[width=3]
"GStreamer"[width=3]
"uboot"[width=2]
"Filesystem"[width=2]
"OSTree Remote Repo"[width=12]
"Python Script"[width=10]

subgraph cluster_buildsystem {
  label="Build System"
  labeljust="l"
  style=dashed

  {rank=same "Sys Def Pipeline"[width=2] "Model Trainer"[width=2] "Root FS Pipeline"[width=3] "AI Libs"[width=2] "App Pipeline"[width=2]}

  "Technician" -> "Sys Def Pipeline"[xlabel="Update System \nDefinition"]

  "Technician" -> "Model Trainer"[xlabel="Update \n Images  "]
  "Technician" -> "Model Trainer"[dir=back, headlabel="Pass/Fail", labelangle=320, labeldistance=4]

  "Technician" -> "Root FS Pipeline"[xlabel="Update\nRoute FS Source Code "]
  "Technician" -> "Root FS Pipeline"[dir=back, headlabel="Pass/Fail", labelangle=320, labeldistance=4]

  "Technician" -> "AI Libs"[xlabel="Update Libs "]
  "Technician" -> "AI Libs"[dir=back, headlabel="Pass/Fail", labelangle=320, labeldistance=4]

  "Technician" -> "App Pipeline"[xlabel="Update\nApp Source Code "]
  "Technician" -> "App Pipeline"[dir=back, headlabel="Pass/Fail", labelangle=320, labeldistance=4]

  "Sys Def Pipeline" -> "System Integration Pipeline"[xlabel="Update System \nDefinition"]
  "Model Trainer" -> "System Integration Pipeline"[xlabel="Model "]
  "Root FS Pipeline" -> "System Integration Pipeline"[xlabel="Update Root FS "]
  "AI Libs" -> "System Integration Pipeline"[xlabel="Update Libs "]
  "App Pipeline" -> "System Integration Pipeline"[xlabel="Update Application "]

  "Technician" -> "System Integration Pipeline"[dir=back, headlabel="Pass/Fail", labelangle=320, labeldistance=4]

  "System Integration Pipeline" -> "OSTree Remote Repo"[xlabel="Push "]
  "System Integration Pipeline" -> "OSTree Remote Repo"[dir=back, xlabel="Version No "]
}

subgraph cluster_vehicle {
	label="Vehicle"
	labeljust="l"
	style=dashed

  {rank=same "Python Script" "OSTree Management Service"}
  {rank=same "Motors" "Camera" "OSTree Remote Repo"}

  "System D" -> "Python Script"[xlabel="Go cmd "]
  "System D" -> "Python Script"[dir=back, xlabel="systemctl cmd \nExit code"]
  "System D" -> "OSTree Management Service"[xlabel="Go cmd "]
  "System D" -> "OSTree Management Service"[dir=back, xlabel="'systemctl restart' cmd "]
  "System D" -> "Argus"[xlabel="Go cmd "]

  "OSTree Management Service" -> "uboot"[xlabel="Boot Location "]
  "OSTree Management Service" -> "Filesystem"[xlabel="Write\nfile updates "]


	"Python Script" -> "GStreamer"[xlabel="Go cmd"]
  "Python Script" -> "GStreamer" [dir=back, xlabel="Environment\nimages"]
  "Python Script" -> "Obstacle Model"[xlabel="Image "]
  "Python Script" -> "Obstacle Model"[dir=back, xlabel="Object\nlocations "]
  "Python Script" -> "Steering Model"[xlabel="Image "]
  "Python Script" -> "Steering Model"[dir=back, xlabel="Steering\ninstructions " ]
	"Python Script"->"Motors"[xlabel="Motor Control "]

  "GStreamer"->"Argus"[xlabel="Config "]
	"GStreamer"->"Argus"[dir=back, xlabel="Environment \nimages"]

  "Argus" -> "Camera"[xlabel="Config "]
  "Argus" -> "Camera"[dir=back, xlabel="Environment \nImages"]
	}

  "OSTree Management Service" -> "OSTree Remote Repo"[weight=10 xlabel="Pull"]
  "OSTree Management Service" -> "OSTree Remote Repo"[dir=back xlabel="File\ndiffs"]
}
